
public class RandomString {
    static String getAlphaNumericString(int n) {
        StringBuilder sb = new StringBuilder(n);
        
        for ( int i = 1; i <= n; i++ ) {
            sb.append((char) ((Math.random() * 95) + 32));
        }
        return sb.toString();
    }
}