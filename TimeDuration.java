class TimeDuration {

    public static long startTime, previousTime;

    public static void init() {
        startTime = System.currentTimeMillis();
        previousTime = startTime;
    }

    public static void durationTimeCheck(String message) {
        long currentTime = System.currentTimeMillis();
        System.out.println(message + ": " + (float) ( currentTime - previousTime ) / 1000 + " Sec; Time from start: " + (float) ( currentTime - startTime ) / 1000 + " Sec.");
        previousTime = currentTime;
    }
}