JFLAGS = -g
JC = javac
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
        TimeDuration.java \
        ListElement.java \
        List.java \
        RandomString.java \
        SortList.java

default: classes

classes: $(CLASSES:.java=.class)

clean:
	$(RM) *.class *.dat
