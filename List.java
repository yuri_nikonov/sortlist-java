import java.io.BufferedWriter;
import java.io.IOException;

class List {
    private ListElement head; // Указатель на первый элемент
    private ListElement tail; // Указатель на последний элемент
    private int index;


    public List() {
        head = null;
        tail = null;
        index = 1;
    }
    
    void addFront(String data) {  // Добавить спереди списка
        ListElement a = new ListElement(); // Создаём новый элемент
        a.value = data;                     // Присваиваем данные
        a.index = this.index;
        if (head == null) {
            head = a;
            tail = a;
        }
        else {
            a.next = head;
            head = a;
        }
        this.index++;
    }
    
    void addBack(String data) {
        ListElement a = new ListElement();
        a.value = data;
        a.index = this.index;
        if (tail == null) {
            head = a;
            tail = a;
        }
        else {
            tail.next = a;
            tail = a;
        }
        this.index++;
    }
    void printList() {
        ListElement t = head;
        while (t != null) {
            System.out.println(t.index + "\t" + t.value);
            t = t.next;
        }
    }
    void delEl(String data) {
        if (head == null)
            return;
        else if (head.value.compareTo(data) == 0) {
            if ( head == tail ) {
                head = null;
                tail = null;
                return;
            }
            head = head.next;
            return;
        }
        
        ListElement t = head;
        while (t.next != null) {
            if (t.next.value.compareTo(data) == 0) {
                if (tail == t.next) {
                    tail = t;
                }
                t.next = t.next.next;
                return;
            }
            t = t.next;
        }
    }
    
    void sortList() {
        ListElement prevaftercurrent, current, min, aftercurrent, prevmin, prevcurrent;
        int countSorted = 0;
        long previousTime = System.currentTimeMillis(), currentTime;
        float durationTimeSec;
        prevcurrent = head;
        current = head;
        prevmin = head;
        while (current != null) {
            min = current;
            aftercurrent = current.next;
            prevaftercurrent = current;
            
            if ((countSorted % 10000) == 0) {
                TimeDuration.durationTimeCheck(countSorted + "/" + (this.index - 1));
            }
            
            while ( aftercurrent != null ) {
                if (aftercurrent.value.compareTo(min.value) < 0) {
                    min = aftercurrent;
                    prevmin = prevaftercurrent;
                }
                prevaftercurrent = aftercurrent;
                aftercurrent = aftercurrent.next;
            }
            if ( min != current ) {
                if (prevcurrent != current) {
                    prevcurrent.next = min;
                }
                else {
                    head = min;
                }
                prevmin.next = current;
                prevaftercurrent = min.next;
                min.next = current.next;
                current.next = prevaftercurrent;
                
                if ( tail == min )
                    tail = current;
                
                current = min;
            }
            prevcurrent = current;
            current = current.next;
            countSorted++;
        }
        
        TimeDuration.durationTimeCheck(countSorted + "/" + (this.index - 1));
    }
    
    void listToFile(BufferedWriter outfile) {
        try {
            ListElement current = head;
            while (current != null) {

                outfile.write(String.format("%06d\t%s\n", current.index, current.value));
                current = current.next;
            }
        }
        catch ( IOException e)
        {
        }
        finally
        {
        }
    }
}