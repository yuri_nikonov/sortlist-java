import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class SortList
{
    public static void main(String[] args) throws Exception
    {
        
        BufferedWriter outfile = null;
        
        TimeDuration.init();
        
        try
        {
            outfile = new BufferedWriter( new FileWriter("outfile.dat"));
            TimeDuration.durationTimeCheck("OutFile opened");

            List a = new List();
            for (int i = 0; i < 70000; i++ )
                a.addBack(RandomString.getAlphaNumericString(127));
            
            TimeDuration.durationTimeCheck("List of strings created");

            a.sortList();
            TimeDuration.durationTimeCheck("List of strings is sorted");

            a.listToFile( outfile );
            TimeDuration.durationTimeCheck("List is saved to file");
        }
        catch ( IOException e)
        {
        }
        finally
        {
            try
            {
                if (outfile != null)
                    outfile.close();
            }
            catch ( IOException e)
            {
            }
        }
    }
}